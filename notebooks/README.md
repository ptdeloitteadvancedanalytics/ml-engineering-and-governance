# Notebooks

In this folder you can see the resulting notebook of the PoC in both jupyter format and html.

Files included are:

- **telecom-customer-churn-prediction-simplified.ipynb** - Result of PoC effort with client
- **churn_use_case.html** - An html page to visualize notebook and share with the business
 