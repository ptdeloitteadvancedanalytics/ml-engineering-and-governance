"""
Main entrypoint: run workflow in order with calls to other entrypoints.
"""
import os.path as op

import click
import mlflow

EXPERIMENT_ID = "EXP_CHURN"


@click.command(help="Run workflow")
@click.option("--data_file", help="CSV data file path.")
@click.option("--out", help="Output folder")
@click.option("--c_param", default=1.0, type=float)
@click.option("--penalty", default='l2', type=str)
@click.option("--max_iter", default=100, type=int)
def run(data_file, out, c_param, penalty, max_iter):
    with mlflow.start_run():
        # Run prepare_data
        submit_prep = mlflow.run(
            ".",
            entry_point="prepare_data",
            parameters={"data_file": data_file, "out_folder": out}
        )

        # Run prepare features
        processed_dataset = op.join(
            get_run_artifact_uri(submit_prep),
            "processed",
            "churn_data_prep.csv"
        )

        submit_feat = mlflow.run(
            ".",
            entry_point="prepare_features",
            parameters={"data_file": processed_dataset, "out_folder": out}
        )

        # Run model
        feature_dataset = op.join(
            get_run_artifact_uri(submit_feat),
            "features",
            "churn_features.csv"
        )

        # Exercise 5: Add the lg model parameters in the main run
        #
        # The model entry point will receive via the mlflow run command a set of parameters (c_param, penalty
        # and max_iter). Add them to the lg_param dictionary in order to pass them through the entry point.

        lg_params = {
            "data_file": feature_dataset,
            "out_folder": out,
            "c": c_param,
            "penalty": penalty,
            "max_iter": max_iter
        }

        mlflow.run(
            ".",
            entry_point="model",
            parameters=lg_params
        )


def get_run_artifact_uri(submitted_run):
    run = mlflow.tracking.MlflowClient().get_run(submitted_run.run_id)
    return run.info.artifact_uri


if __name__ == '__main__':
    run()
