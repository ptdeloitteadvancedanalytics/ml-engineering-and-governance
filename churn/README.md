# Workshop Scenario

## Motivation 

Most data science projects currently culminate with the delivery of a notebook that, by itself, adds few 
value to the client. Intricacy lies in integrating a model, with the client’s technological ecosystem in 
a production environment and, thus, delivering tangible value; 

A machine learning end-to-end solution should be scalable, fault tolerant and should provide a way of 
having governance over experiments and deployed models.

This exercise focus on providing a view on how end-to-end machine learning could be delivered. Leveraging 
on [mlflow](https://mlflow.org/), an open source machine learning platform that provides a set of 
principles that helps accelerate model development, provides governance over experiments and models, 
with scalability and fault tolerance as part of its DNA. 


## Scope

You are part of a Deloitte's data science team that just had 2 months to deliver a PoC to address a 
customer churn use case in one of the largest telcos operating in Europe. The result of the PoC was a 
notebook that the client found very eye opening and convinced them to peruse further endeavors in the 
data science scope. 

The client asked Deloitte's team to now take action and turn the notebook into an actual production ready 
workflow.

Your manager advised you to explore `mlflow` and turn the notebook into a workflow orchestrated by `mlflow`. 


## The Use Case: Telecom Churn

Customer churn, customer turnover, or customer defection, is the loss of clients or customers.

Telephone service companies, Internet service providers, pay TV companies, insurance firms, and alarm 
monitoring services, often use customer churn analysis and customer churn rates as one of their key 
business metrics because the cost of retaining an existing customer is far less than acquiring a new 
one. Companies from these sectors often have customer service branches which attempt to win back 
defecting clients, because recovered long-term customers can be worth much more to a company than newly 
recruited clients.

Companies usually make a distinction between voluntary churn and involuntary churn. Voluntary churn 
occurs due to a decision by the customer to switch to another company or service provider, involuntary 
churn occurs due to circumstances such as a customer"s relocation to a long-term care facility, death, 
or the relocation to a distant location. In most applications, involuntary reasons for churn are excluded 
from the analytical models. Analysts tend to concentrate on voluntary churn, because it typically occurs 
due to factors of the company-customer relationship which companies control, such as how billing 
interactions are handled or how after-sales help is provided.

Predictive analytics use churn prediction models that predict customer churn by assessing their 
propensity of risk to churn. Since these models generate a small prioritized list of potential defectors, 
they are effective at focusing customer retention marketing programs on the subset of the customer base 
who are most vulnerable to churn.


## The exercise

This MLproject aims to be a fully self-contained example of how to chain together multiple different 
MLflow runs which each encapsulate a transformation or training step, allowing a clear definition of the 
interface between the steps, as well as allowing for caching and reuse of the intermediate results.

Some of the files that you"ll find under this project:

- **MLproject**: Contains definition of this project. Contains only one entry point to train the model.

- **env.yaml**: Defines project dependencies.

- **main.py**: Driver run, will run the steps in order, passing the results of one to the next. 
Additionally, this run will attempt to determine if a sub-run has already been executed successfully 
with the same parameters and, if so, reuse the cached results.

There are a few stages to this workflow in the `ops` folder:

1. **prepare_data.py**: responsible for pre-processing tasks

2. **prepare_features.py**: responsible for feature engineering

3. **lgmodel.py**: responsible for training and evaluating the model


#### 1. Build your workflow steps

Starting from the notebook you successfully delivered during the PoC, segregate it into the different 
typical stages of a machine learning workflow:

- Load Data (stage 1)
- Preprocess Data (stage 1)
- Feature Engineering (stage 2)
- Train (stage 3)
- Evaluate (stage 3)

You'll find a file per stage that requires certain tasks for completion.

#### 2. Log for governance and run mlflow

Using [mlflow python API](https://www.mlflow.org/docs/latest/python_api/index.html), decide what you 
should log for governance.

After you are confident, run the workflow by opening a wsl shell, moving into this repo root directory 
and run `mlflow run churn`. An example `mlflow` run can be:

```bash
$ cd ~/workflow/git/ml-engineering-and-governance
$ mlflow run churn -P data_file=~/workspace/data/input/churn_data_sample.csv -P out_folder=~/workspace/data/output/ -P c_param=0.1 -P penalty=l2 -P max_iter=30
```

In a different wsl shell, move into the root of this repo, i.e., 
`~/workflow/git/ml-engineering-and-governance`, and run `mlflow ui`. The mlflow UI will then start being 
served from port 5000 on localhost. Go to you preferred browser and access `http://localhost:5000`. This 
will enable you tho check and compare your runs.

```bash
$ cd ~/workflow/git/ml-engineering-and-governance
$ mlflow ui
```

#### 3. Serve your model

When you're happy with your model's performance, you can serve it by doing:

```bash
$ mlflow models serve -m ~/workspace/git/ml-engineering-and-governance/mlruns/0/__ARTIFACT_ID__/artifacts/lg_0.2/ -p 1234
```

Check where it is located in the mlflow UI and replace the path in the above command.

Your model is now being served on your local machine under a REST API on port 1234, from now on you 
can query it by passing it some sample data and see the predictions. The following example uses curl 
to send a JSON-serialized pandas DataFrame with the split orientation to the model server:

```bash
$ curl -X POST -H "Content-Type:application/json; format=pandas-split" --data '{"columns":["gender", "SeniorCitizen", "Partner", "Dependents", "PhoneService", "OnlineSecurity", "OnlineBackup", "DeviceProtection", "TechSupport", "StreamingTV", "StreamingMovies", "PaperlessBilling", "MultipleLines_No", "MultipleLines_No phone service", "MultipleLines_Yes", "InternetService_DSL", "InternetService_Fiber optic", "InternetService_No", "Contract_Month-to-month", "Contract_One year", "Contract_Two year", "PaymentMethod_Bank transfer (automatic)", "PaymentMethod_Credit card (automatic)", "PaymentMethod_Electronic check", "PaymentMethod_Mailed check", "tenure", "MonthlyCharges", "TotalCharges"],"data":[[0,0,1,0,0,0,1,0,0,0,0,1,0,1,0,1,0,0,1,0,0,0,0,1,0,-1.28,-1.16,-0.99]]}' http://127.0.0.1:1234/invocations
```

You can change the data in the POST request to see different responses. 

This API would easily be integrated with any tech ecosystem and could easily be deployed in a scalable 
environment such as the cloud or a kubernetes cluster.

### Exercise steps

#### Exercise 1. Adding environment dependencies

The notebook has several packages that it imports to produce the final module. The `env.yml` file will centralize all
pipeline dependencies in order to insure that, when the pipeline is ran, a correct replica of the environment can
be replicated, without any dependencies.
The environment is created using Anaconda, which will handle all dependencies.

For this exercise, add the following dependencies:
 - `pandas` (any version of 0.25)
 - `mlflow` (version 1.4.0)
 - `click` (version 7.0
 
**Location:** `churn/env.yml`

#### Exercise 2: Add the prep data code

This section will handle the prep data section of the notebook (section 2. Data manipulation, up until the
feature engineering). Migrate the notebook process into this method and return a Pandas dataframe containing
the result of those actions.

**Location:** `churn/ops/prepare_data.py`

#### Exercise 3. Log the feature file and correlation matrix

As part of the output of the feature engineering phase, a new file containing all the features required for
training is created, as well as a correlation matrix between each feature. This information needs to be
logged in order to verify the exact output that this step outputs (e.g., for audit proposes; for other data
scientists to review feature engineering output).

To do so, use the `mlflow.log_artifact` method to log both the prep_file and the corr_image

**Location:** `churn/ops/prepare_features.py`

#### Exercise 4. Log the model and other artifacts

After a model is trained, it must be stored and tracked, along with its byproducts (e.g., metrics).
 - Log the model, use the `sklearn` package from `mlflow` using: `mlflow.sklearn.log_model(model, f"lg_{c_param}")`
 - Log the solver using: `mlflow.log_param("solver", model.solver)`
 - Look at the code and identify what other outputs can or should be logged

**Location:** `churn/ops/lgmodel.py`

#### Exercise 5: Add the lg model parameters in the main run

The model entry point will receive via the `mlflow run` command a set of parameters (`c_param`, `penalty`
and `max_iter`). Add them to the `lg_param` dictionary in order to pass them through the entry point.

**Location:** `churn/main.py`

## Code structure

Main methods are in the `main.py`, while the `ops` folder contains the operations for each stage in the
machine learning pipeline.

```
+-- ops
|   +-- lgmodel.py
|   +-- prepare_data.py
|   +-- prepare_features.py
+-- env.yml
+-- main.py
+-- MLProject
```

