"""
Load data from csv.
"""

import os.path as op

import click
import mlflow
from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

PREP_FILE_NAME = "churn_data_prep.csv"


def create_spark_session():
    """
    Creates a Spark Session
    """
    conf = SparkConf()

    conf.set("spark.executor.memory", "25g")
    conf.set("spark.executor.cores", "4")
    conf.set("spark.executor.instances", "12")
    conf.set("spark.driver.memory", "5g")
    # Spark options
    conf.set("spark.yarn.executor.memoryOverhead", "4000")
    conf.set("spark.yarn.driver.memoryOverhead", "4000")
    conf.set("spark.network.timeout", "800s")
    conf.set("spark.executor.heartbeatInterval", "60s")

    ss = SparkSession.builder.config(conf=conf).enableHiveSupport().getOrCreate()

    return ss


def preprocess_data(df):
    """

    """
    # Replacing spaces with null values in total charges column
    op1 = F.when(F.col("TotalCharges") == " ", float("nan")).otherwise(F.col("TotalCharges"))
    df = df.withColumn("TotalCharges", op1)

    # Dropping null values from total charges column which contain .
    # 15% missing data
    df = df.filter(df.TotalCharges.isNotNull())

    # Convert to float type
    df = df.withColumn("TotalCharges", df["TotalCharges"].cast(T.DoubleType()))

    # Replace 'No internet service' to No for the following columns
    replace_cols = [
        'OnlineSecurity',
        'OnlineBackup',
        'DeviceProtection',
        'TechSupport',
        'StreamingTV',
        'StreamingMovies'
    ]

    for c in replace_cols:
        op2 = F.when(F.col(c) == "No internet service", "No").otherwise(F.col(c))
        df = df.withColumn(c, op2)

    # Replace values in 'SeniorCitizen'
    op3 = F.when(F.col("SeniorCitizen") == "1", "Yes")
    op4 = F.when(F.col("SeniorCitizen") == "0", "No")

    df = df.withColumn("SeniorCitizen", op3)
    df = df.withColumn("SeniorCitizen", op4)

    return df


@click.command(help="Preprocess data from csv file.")
@click.option("--data_file", help="CSV data file path.")
@click.option("--out", help="Output folder")
def prepare_data(data_file, out):
    with mlflow.start_run():
        spark = create_spark_session()
        telcom = spark.read.csv(data_file, header="True")

        # method where to include notebook code
        df = preprocess_data(telcom)

        # Save Processed File
        prep_file = op.join(out, PREP_FILE_NAME)

        pdf = df.select("*").toPandas()
        pdf.to_csv(prep_file, index=False)

        # Log artifact
        mlflow.log_artifact(prep_file, "processed")


if __name__ == '__main__':
    prepare_data()
