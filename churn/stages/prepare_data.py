"""
Load data from csv.
"""

import os.path as op

import click
import mlflow
import numpy as np
import pandas as pd

PREP_FILE_NAME = "churn_data_prep.csv"


@click.command(help="Preprocess data from csv file.")
@click.option("--data_file", help="CSV data file path.")
@click.option("--out", help="Output folder")
def prepare_data(data_file, out):
    with mlflow.start_run():
        telcom = pd.read_csv(data_file)

        # method where to include notebook code
        df = preprocess_data(telcom)

        # Save Processed File
        prep_file = op.join(out, PREP_FILE_NAME)

        df.to_csv(prep_file, index=False)

        # Log artifact
        mlflow.log_artifact(prep_file, "processed")


def preprocess_data(df):
    # Exercise 2: Add the prep data code
    #
    # This section will handle the prep data section of the notebook (section 2. Data manipulation, up until the
    # feature engineering). Migrate the notebook process into this method and return a Pandas dataframe containing
    # the result of those actions.

    telcom = df.copy()

    # Replacing spaces with null values in total charges column
    telcom['TotalCharges'] = telcom["TotalCharges"].replace(" ", np.nan)

    # Dropping null values from total charges column which contain .
    # 15% missing data
    telcom = telcom[telcom["TotalCharges"].notnull()]
    telcom = telcom.reset_index()[telcom.columns]

    # Convert to float type
    telcom["TotalCharges"] = telcom["TotalCharges"].astype(float)

    # Replace 'No internet service' to No for the following columns
    replace_cols = [
        'OnlineSecurity',
        'OnlineBackup',
        'DeviceProtection',
        'TechSupport',
        'StreamingTV',
        'StreamingMovies'
    ]

    for i in replace_cols:
        telcom[i] = telcom[i].replace({'No internet service': 'No'})

    # Replace values in 'SeniorCitizen'
    telcom["SeniorCitizen"] = telcom["SeniorCitizen"].replace(
        {1: "Yes", 0: "No"}
    )

    return telcom


if __name__ == '__main__':
    prepare_data()
