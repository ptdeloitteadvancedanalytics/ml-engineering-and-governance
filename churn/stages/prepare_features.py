"""
Load data from csv.
"""

import os.path as op

import click
import matplotlib.pyplot as plt
import mlflow
import pandas as pd
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler

FEAT_FILE_NAME = "churn_features.csv"


@click.command(help="Prepare features for model.")
@click.option("--data_file", help="CSV data file path.")
@click.option("--out", help="Output folder")
def run(data_file, out):
    with mlflow.start_run():
        telcom = pd.read_csv(data_file)

        # method where to include notebook code
        df = prepare_features(telcom)

        # Corr plot
        corr = df.corr()

        corr_image = op.join(out, "feature_corr.png")

        fig = plt.figure(figsize=(10, 8))
        sns.heatmap(corr)
        plt.title("Correlation Matrix")
        plt.ylabel("True label")
        plt.xlabel("Predicted label")

        fig.savefig(corr_image)

        # Save Processed File
        prep_file = op.join(out, FEAT_FILE_NAME)

        df.to_csv(prep_file, index=False)

        # Log artifacts

        # Exercise 3. Log the feature file and correlation matrix
        #
        # As part of the output of the feature engineering phase, a new file containing all the features required for
        # training is created, as well as a correlation matrix between each feature. This information needs to be
        # logged in order to verify the exact output that this step outputs (e.g., for audit proposes; for other data
        # scientists to review feature engineering output).
        #
        # To do so, use the mlflow.log_artifact method to log both the prep_file and the corr_image

        mlflow.log_artifact(prep_file, "features")
        mlflow.log_artifact(corr_image, "corr_matrix")


def prepare_features(df):
    telcom = df.copy()

    ID = 'customerID'
    TARGET = 'Churn'

    # categorical columns
    cat_cols = telcom.nunique()[telcom.nunique() < 6].keys().tolist()
    cat_cols = [x for x in cat_cols if x not in [TARGET]]

    # numerical columns
    num_cols = [
        x for x in telcom.columns if x not in cat_cols + [TARGET] + [ID]
    ]

    # Binary columns with 2 values
    bin_cols = telcom.nunique()[telcom.nunique() == 2].keys().tolist()

    # Columns more than 2 values
    multi_cols = [i for i in cat_cols if i not in bin_cols]

    # Label encoding Binary columns
    le = LabelEncoder()
    for i in bin_cols:
        telcom[i] = le.fit_transform(telcom[i])

    # Duplicating columns for multi value columns
    telcom = pd.get_dummies(data=telcom, columns=multi_cols)

    # Scaling Numerical columns
    std = StandardScaler()
    scaled = std.fit_transform(telcom[num_cols])
    scaled = pd.DataFrame(scaled, columns=num_cols)

    # Dropping original  values merging scaled values for numerical columns
    telcom = telcom.drop(columns=num_cols, axis=1)
    telcom = telcom.merge(
        scaled,
        left_index=True,
        right_index=True,
        how="left"
    )

    return telcom


if __name__ == '__main__':
    run()
