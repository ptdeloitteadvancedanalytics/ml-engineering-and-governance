"""
Train and evaluate logistic regression model.
"""
import os.path as op

import click
import matplotlib.pyplot as plt
import mlflow
import mlflow.sklearn
# %%
import pandas as pd
import seaborn as sns
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve, roc_auc_score, confusion_matrix
from sklearn.model_selection import train_test_split

# %%
ID = 'customerID'
TARGET = 'Churn'
RANDOM_STATE = 111
OUT_FOLDER = op.join(op.dirname(__file__), "out")


# %%
@click.command(help="Train log reg model.")
@click.option("--data_file")
@click.option("--out", help="Output folder")
@click.option("--c_param", default=1.0, type=float)
@click.option("--penalty", default='l2', type=str)
@click.option("--max_iter", default=100, type=int)
def run(data_file, out, c_param, penalty, max_iter):
    with mlflow.start_run():
        # Read processed data file
        df = pd.read_csv(data_file)

        # Split Dataset
        COLS = [i for i in df.columns if i not in [ID] + [TARGET]]
        X = df[COLS]
        y = df[TARGET]

        train_X, test_X, train_Y, test_Y = train_test_split(
            X, y,
            test_size=.25,
            random_state=RANDOM_STATE
        )

        # Model
        model = LogisticRegression(
            penalty=penalty,
            C=c_param,
            max_iter=max_iter,
            random_state=RANDOM_STATE,
            solver='lbfgs'
        )

        model.fit(train_X, train_Y)
        print(f'Logistic Regression model score {model.score(test_X, test_Y)}.')

        # Evaluation
        predictions = model.predict(test_X)
        probabilities = model.predict_proba(test_X)[:, 1]

        # AUC ROC
        auc = roc_auc_score(test_Y, probabilities)
        print(f'ROC AUC Score for model is {auc}')

        # ROC Curve - Plot
        roc_image = op.join(out, f"lg_{c_param}_roc_curve.png")
        fig = plt.figure(figsize=(10, 8))
        fpr, tpr, _ = roc_curve(test_Y, probabilities)
        sns.lineplot(fpr, tpr)
        sns.lineplot([0, 1], [0, 1])
        plt.title("ROC curve")

        fig.savefig(roc_image)

        # Confusion Matrix - Plot
        cm = confusion_matrix(test_Y, predictions)

        cm_image = op.join(out, f"lg_{c_param}_confusion_matrix.png")
        fig = plt.figure(figsize=(10, 8))
        sns.heatmap(cm)
        plt.title("Confusion Matrix")
        plt.ylabel("True label")
        plt.xlabel("Predicted label")

        fig.savefig(cm_image)

        # Log model

        # Exercise 4. Log the model and other artifacts
        #
        # After a model is trained, it must be stored and tracked, along with its byproducts (e.g., metrics).
        # >> Log the model, use the sklearn package from mlflow using: mlflow.sklearn.log_model(model, f"lg_{c_param}")
        # >> Log the solver using: mlflow.log_param("solver", model.solver)
        # >> Look at the code and identify what other outputs can or should be logged

        mlflow.sklearn.log_model(model, f"lg_{c_param}")

        # Log model performance metrics
        mlflow.log_metric("roc_auc_score", auc)

        # Log model params: one that is not an argument or default.
        mlflow.log_param("solver", model.solver)

        # Log plots with model analysis
        mlflow.log_artifact(roc_image, "roc_auc")
        mlflow.log_artifact(cm_image, "confusion_matrix")


if __name__ == '__main__':
    run()
