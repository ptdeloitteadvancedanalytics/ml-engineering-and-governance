# PT/NL Knowledge Share


## Prerequisites for running on Windows machines


#### 1. Install [WSL - Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10). 

1. Follow the guide: [WSL - Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10). We recommend Ubuntu latest available version;


#### 2. Install Anaconda3 on WSL

1. Go to https://repo.continuum.io/archive to find the list of Anaconda releases;
2. Select the release you want. If you have a 64-bit computer, choose the latest release ending in x86_64.sh. If you have a 32-bit computer, go with the x86.sh version;
3. From the wsl terminal run `wget https://repo.continuum.io/archive/[YOUR VERSION]`. Example: `$ wget https://repo.continuum.io/archive/Anaconda3-5.2.0-Linux-x86_64.sh`
4. Run the installation script: `$ bash Anaconda[YOUR VERSION].sh` ($ bash Anaconda3-5.2.0-Linux-x86_64.sh)
5. Close the terminal and reopen it to reload .bash configs.
6. To test that it worked, run `$ which python`. It should print a path that has anaconda in it (e.g. /home/joadinis/anaconda3/bin/python). If it doesn't have anaconda in the path, do the next step. Otherwise, you're go to go!
7. Manually add the Anaconda bin folder to your PATH. To do this, add "export PATH=/home/joadinis/anaconda3/bin:$PATH" to the bottom of your `~/.bashrc file`.


#### 3. Install mlflow on anaconda environment

1. Open a WSL terminal;
2. Install mlflow by running `pip install mlflow`
3. Check mlflow was installed in anaconda by running `which mlflow`, it should print a path that has anaconda in it.


#### 4. Install AWS CLI on WSL (Optional if Tracking is hosted in AWS)

1. Open a WSL terminal;
2. Run `pip install awscli --upgrade --user`;
3. Run `aws configure` and provide the access keys that the monitor of the lab should have provided you.


## Environment setup

Run the following commands to set up the working environment for the workshop:

#### 1. Create folder structure

```bash
$ mkdir ~/workspace
$ mkdir ~/workspace/data
$ mkdir ~/workspace/data/input
$ mkdir ~/workspace/data/output
$ mkdir ~/workspace/git
$ mkdir ~/workspace/notebooks
```

#### 2. Clone repository

```bash
$ cd ~/workspace/git
$ git clone https://__USER__@bitbucket.org/ptdeloitteadvancedanalytics/ml-engineering-and-governance.git
$ git checkout baseline
$ cd ~
```

#### 3. Move files outside of repository

```bash
$ cp ~/workspace/git/ml-engineering-and-governance/data/*.csv ~/workspace/data/input
$ cp ~/workspace/git/ml-engineering-and-governance/notebooks/* ~/workspace/notebooks
```
