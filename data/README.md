# Description of the files

Data that was used during the PoC.

- **churn_data.csv** - The full dataset used during PoC phase
- **churn_data_sample.csv** - A subset of the dataset used during PoC phase
